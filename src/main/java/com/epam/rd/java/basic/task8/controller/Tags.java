package com.epam.rd.java.basic.task8.controller;

//public enum Tags {chargingStations, chargingStation, name, type, brand, visualParameters, topColour, bottomColour}
public enum Tags {
    CHARGINGSTATIONS("chargingStations"),
    CHARGINGSTATION("chargingStation"),
    NAME("name"),
    TYPE("type"),
    BRAND("brand"),
    VISUALPARAMETERS("visualParameters"),
    TOPCOLOUR("topColour"),
    BOTTOMCOLOUR("bottomColour");

    private String value;

    private Tags(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }
}