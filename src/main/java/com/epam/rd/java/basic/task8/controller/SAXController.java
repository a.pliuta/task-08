package com.epam.rd.java.basic.task8.controller;

import com.epam.rd.java.basic.task8.step1.ChargerStation;
import com.epam.rd.java.basic.task8.step1.ChargerStations;
import com.epam.rd.java.basic.task8.step1.VisualParameters;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.AttributesImpl;
import org.xml.sax.helpers.DefaultHandler;

import javax.xml.XMLConstants;
import javax.xml.parsers.*;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.sax.SAXTransformerFactory;
import javax.xml.transform.sax.TransformerHandler;
import javax.xml.transform.stream.StreamResult;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Comparator;
import java.util.Locale;


/**
 * Controller for SAX parser.
 */
public class SAXController extends DefaultHandler {
	
	private String xmlFileName;
	private ChargerStations chargerStations = new ChargerStations();

	public static final String CDATA = "CDATA";
	public static final String EMPTY = "";

	public SAXController(String xmlFileName) {
		this.xmlFileName = xmlFileName;
	}

	public ChargerStations parseXML() throws IOException, SAXException, ParserConfigurationException {
		SAXParserFactory factory = SAXParserFactory.newInstance();
		factory.setFeature("http://apache.org/xml/features/disallow-doctype-decl", true);
		factory.setNamespaceAware(true);
		SAXParser parser = factory.newSAXParser();
		XMLHandler handler = new XMLHandler();
		parser.parse(xmlFileName, handler);
		return chargerStations;
	}

	public class XMLHandler extends DefaultHandler{
		Tags tag;
		ChargerStation chargerStation;
		VisualParameters vP;

		@Override
		public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {
			tag = Tags.valueOf(localName.toUpperCase());
			//System.out.println(tag);
			switch(tag){
				case CHARGINGSTATION:
					chargerStation = new ChargerStation();
					break;
				case VISUALPARAMETERS:
					vP = new VisualParameters();
					break;
				default:
					break;
			}
		}

		@Override
		public void endElement(String uri, String localName, String qName) throws SAXException {
			tag = Tags.valueOf(localName.toUpperCase());
			switch(tag){
				case CHARGINGSTATION:
				chargerStations.getList().add(chargerStation);
				break;
				case VISUALPARAMETERS:
					chargerStation.setvP(vP);
					break;
				default:
					break;
			}
		}

		@Override
		public void characters(char[] ch, int start, int length) throws SAXException {
			String s = new String(ch, start, length).trim();
			if (!s.isBlank()){
				switch(tag){
					case NAME:
						chargerStation.setName(s);
						break;
					case TYPE:
						chargerStation.setType(s);
						break;
					case BRAND:
						chargerStation.setBrand(s);
						break;
					case TOPCOLOUR:
						vP.setTopColour(s);
						break;
					case BOTTOMCOLOUR:COLOUR:
						vP.setBottomColour(s);
						break;
					default:
						break;
				}
			}
		}
	}
	public static void Sorting(ChargerStations chargerStations){
		chargerStations.getList().sort(Comparator.comparing(ChargerStation::getName));
	}

	public void createXML(ChargerStations chargerStations, String outputXmlFile) throws ParserConfigurationException, TransformerException, IOException, SAXException {
		this.chargerStations = chargerStations;

		SAXTransformerFactory factory = (SAXTransformerFactory) TransformerFactory.newInstance();
		factory.setAttribute(XMLConstants.ACCESS_EXTERNAL_DTD, EMPTY);
		factory.setAttribute(XMLConstants.ACCESS_EXTERNAL_STYLESHEET, EMPTY);
		TransformerHandler hd = factory.newTransformerHandler();
		Transformer serializer = hd.getTransformer();
		serializer.setOutputProperty(OutputKeys.INDENT, "yes");
		hd.setResult(new StreamResult(outputXmlFile));
		buildDocument(hd);
	}

	private void buildDocument(TransformerHandler hd) throws SAXException {
		AttributesImpl atts = new AttributesImpl();

		atts.addAttribute(EMPTY, EMPTY, "xmlns", CDATA, "http://www.nd.ua");
		atts.addAttribute(EMPTY, EMPTY, "xmlns:xsi", CDATA, "http://www.w3.org/2001/XMLSchema-instance");
		atts.addAttribute(EMPTY, EMPTY, "xsi:schemaLocation", CDATA, "http://www.nd.ua input.xsd ");

		hd.startDocument();
		hd.startElement(EMPTY, EMPTY, "chargingStations", atts);
		for(ChargerStation chargerStation : chargerStations.getList()){
			buildChild(hd, atts, chargerStation);
		}
		hd.endElement(EMPTY, EMPTY, "chargingStations");
		hd.endDocument();
	}

	private void buildChild(TransformerHandler hd, AttributesImpl atts, ChargerStation chargerStation) throws SAXException {
		atts.clear();
		hd.startElement(EMPTY, EMPTY, "chargingStation", atts);
		builElement(hd, atts, "name", chargerStation.getName());
		builElement(hd, atts, "type", chargerStation.getType().getName());
		builElement(hd, atts, "brand", chargerStation.getBrand());
		builVP(hd, atts, chargerStation.getvP());
		hd.endElement(EMPTY, EMPTY, "chargingStation");

	}

	private void builElement(TransformerHandler hd, AttributesImpl atts, String name, String value) throws SAXException {
		hd.startElement(EMPTY, EMPTY, name, atts);
		hd.characters(value.toCharArray(), 0,value.length());
		hd.endElement(EMPTY, EMPTY, name);
	}

	private void builVP(TransformerHandler hd, AttributesImpl atts, VisualParameters vP) throws SAXException {
		hd.startElement(EMPTY, EMPTY, "visualParameters", atts);
		builElement(hd, atts, "topColour", vP.getTopColour());
		builElement(hd, atts, "bottomColour", vP.getBottomColour());
		hd.endElement(EMPTY, EMPTY, "visualParameters");
	}
}