package com.epam.rd.java.basic.task8.step1;

public class ChargerStation {
    private String name;
	private Type type;
	private String brand;
	private VisualParameters vP;

	public ChargerStation() {
	}

	public ChargerStation(String name, String type, String brand, VisualParameters vP) {
		this.name = name;
		this.type = findType(type);
		this.brand = brand;
		this.vP = vP;
	}

	private Type findType(String type){
		for( Type t: Type.values()){
			if (t.getName().equals(type)) return t;
		}
		return null;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Type getType() {
		return type;
	}

	public void setType(String type) {
		this.type = findType(type);
	}

	public String getBrand() {
		return brand;
	}

	public void setBrand(String brand) {
		this.brand = brand;
	}

	public VisualParameters getvP() {
		return vP;
	}

	public void setvP(VisualParameters vP) {
		this.vP = vP;
	}

	@Override
	public String toString() {
		return "ChargerStation{" +
				"name='" + name + '\'' +
				", type=" + type +
				", brand='" + brand + '\'' +
				", vP=" + vP +
				'}';
	}
}
