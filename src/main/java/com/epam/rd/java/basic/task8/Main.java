package com.epam.rd.java.basic.task8;

import com.epam.rd.java.basic.task8.controller.*;
import com.epam.rd.java.basic.task8.step1.ChargerStations;

public class Main {
	
	//public static void main(String[] args) throws Exception {
	public static String[] main(String[] args) throws Exception {
		String [] out;
		out = new String[3];

		if (args.length != 2) {
			return null;
		}
		
		String xmlFileName = args[0];
		System.out.println("Input ==> " + xmlFileName);

		String xsdFileName = args[1];
		System.out.println("InputXSD ==> " + xsdFileName);

		ValidatorXML.validation(xmlFileName, xsdFileName);
		
		////////////////////////////////////////////////////////
		// DOM
		////////////////////////////////////////////////////////
		
		// get container
		DOMController domController = new DOMController(xmlFileName);
		ChargerStations csDOM = domController.parseXML();
		System.out.println(csDOM);


		// PLACE YOUR CODE HEREDOM
		domController.Sorting(csDOM);
		System.out.println(csDOM);
		
		// save
		String outputXmlFile = "output.dom.xml";
		domController.createXML(csDOM,outputXmlFile);
		out[0] = outputXmlFile;

		////////////////////////////////////////////////////////
		// SAX
		////////////////////////////////////////////////////////
		
		// get
		SAXController saxController = new SAXController(xmlFileName);
		ChargerStations csSAX = saxController.parseXML();
		System.out.println(csSAX);
		
		// sort  (case 2)
		saxController.Sorting(csSAX);
		System.out.println(csSAX);
		
		// save
		outputXmlFile = "output.sax.xml";
		saxController.createXML(csSAX,outputXmlFile);
		out[1] = outputXmlFile;
		// PLACE YOUR CODE HERE
		
		////////////////////////////////////////////////////////
		// StAX
		////////////////////////////////////////////////////////
		
		// get
		STAXController staxController = new STAXController(xmlFileName);
		ChargerStations csSTAX = staxController.parseXML();
		System.out.println(csSTAX);
		
		// sort  (case 3)
		staxController.Sorting(csSTAX);
		System.out.println(csSTAX);
		
		// save
		outputXmlFile = "output.stax.xml";
		staxController.createXML(csSTAX,outputXmlFile);
		out[2] = outputXmlFile;
		// PLACE YOUR CODE HERE
		return out;

	}

//	public static class Demo {
//
//		public static void main(String[] args) throws Exception {
//			Main.main(new String[] { "input.xml" });
//		}
//
//	}
}
