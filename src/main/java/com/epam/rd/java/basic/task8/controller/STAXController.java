package com.epam.rd.java.basic.task8.controller;

import com.epam.rd.java.basic.task8.step1.ChargerStation;
import com.epam.rd.java.basic.task8.step1.ChargerStations;
import com.epam.rd.java.basic.task8.step1.VisualParameters;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import javax.xml.XMLConstants;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import javax.xml.stream.*;
import javax.xml.stream.events.EndElement;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.sax.SAXTransformerFactory;
import javax.xml.transform.sax.TransformerHandler;
import javax.xml.transform.stream.StreamResult;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Comparator;

/**
 * Controller for StAX parser.
 */
public class STAXController extends DefaultHandler {

	private String xmlFileName;

	public STAXController(String xmlFileName) {
		this.xmlFileName = xmlFileName;
	}

	private ChargerStations chargerStations = new ChargerStations();

	public static final String ONETAB = "\n	";
	public static final String TWOTABS = "\n		";
	public static final String THREETABS = "\n			";
	public static final String LR = "\n";

	public ChargerStations parseXML() throws IOException, SAXException, ParserConfigurationException, XMLStreamException {

		XMLInputFactory factory = XMLInputFactory.newInstance();
		factory.setProperty(XMLInputFactory.SUPPORT_DTD, false);
		XMLEventReader reader = factory.createXMLEventReader(new FileInputStream(xmlFileName));
		ChargerStation chargerStation = new ChargerStation();
		VisualParameters vp = new VisualParameters();


		while (reader.hasNext()) {

			XMLEvent event = reader.nextEvent();

			if (event.isStartElement()) {
				parseElement(reader, event, chargerStation, vp);
			}
			if (event.isEndElement()){
				EndElement endElement = event.asEndElement();
				Tags tag = Tags.valueOf(endElement.getName().getLocalPart().toUpperCase());
				switch (tag){
					case VISUALPARAMETERS:
						chargerStation.setvP(vp);
						vp = new VisualParameters();
						break;
					case CHARGINGSTATION:
						chargerStations.getList().add(chargerStation);
						chargerStation = new ChargerStation();
						break;
					default:
						break;
				}
			}
		}
		return chargerStations;
	}

	private void parseElement(XMLEventReader reader, XMLEvent event, ChargerStation chargerStation, VisualParameters vp) throws XMLStreamException {
		StartElement st = event.asStartElement();
		Tags tag = Tags.valueOf(st.getName().getLocalPart().toUpperCase());
		switch (tag) {
			case NAME:
				event = reader.nextEvent();
				chargerStation.setName(event.asCharacters().getData());
				break;
			case TYPE:
				event = reader.nextEvent();
				chargerStation.setType(event.asCharacters().getData());
				break;
			case BRAND:
				event = reader.nextEvent();
				chargerStation.setBrand(event.asCharacters().getData());
				break;
			case TOPCOLOUR:
				event = reader.nextEvent();
				vp.setTopColour(event.asCharacters().getData());
				break;
			case BOTTOMCOLOUR:
				event = reader.nextEvent();
				vp.setBottomColour(event.asCharacters().getData());
				break;
			default:
				break;
		}
	}

	public static void Sorting(ChargerStations chargerStations){
		//chargerStations.getList().sort(Comparator.comparing(ChargerStation::getType).thenComparing(obj -> obj.getvP().getBottomColour()));
		chargerStations.getList().sort(Comparator.comparing(obj -> obj.getvP().getBottomColour()));
		//chargerStations.getList().sort(Comparator.comparing(ChargerStation::getName, Comparator.reverseOrder()));
	}

	public void createXML(ChargerStations chargerStations, String outputXmlFile) throws ParserConfigurationException, TransformerException, IOException, SAXException, XMLStreamException {
		XMLOutputFactory factory = XMLOutputFactory.newInstance();
		XMLStreamWriter streamWriter = factory.createXMLStreamWriter(new FileWriter(outputXmlFile));

		streamWriter.writeStartDocument("UTF-8","1.0");

		streamWriter.writeCharacters(LR);
		streamWriter.writeStartElement("chargingStations");
		streamWriter.writeAttribute("xmlns", "http://www.nd.ua");
		streamWriter.writeAttribute("xmlns:xsi", "http://www.w3.org/2001/XMLSchema-instance");
		streamWriter.writeAttribute("xsi:schemaLocation", "http://www.nd.ua input.xsd ");
		streamWriter.writeCharacters(LR);

		for(ChargerStation chargerStation: chargerStations.getList()){
			buildChild(streamWriter, chargerStation);
		}
		streamWriter.writeCharacters(LR);
		streamWriter.writeEndElement();
		streamWriter.writeEndDocument();
		streamWriter.flush();
		streamWriter.close();
	}

	private void buildChild(XMLStreamWriter streamWriter, ChargerStation chargerStation) throws XMLStreamException {
		streamWriter.writeCharacters(ONETAB);
		streamWriter.writeStartElement("chargingStation");
		buildElement(streamWriter, "name", chargerStation.getName(), TWOTABS);
		buildElement(streamWriter, "type", chargerStation.getType().getName(), TWOTABS);
		buildElement(streamWriter, "brand", chargerStation.getBrand(), TWOTABS);
		streamWriter.writeCharacters(TWOTABS);
		streamWriter.writeStartElement("visualParameters");
		buildElement(streamWriter, "topColour", chargerStation.getvP().getTopColour(), THREETABS);
		buildElement(streamWriter, "bottomColour", chargerStation.getvP().getBottomColour(), THREETABS);
		streamWriter.writeCharacters(TWOTABS);
		streamWriter.writeEndElement();
		streamWriter.writeCharacters(ONETAB);
		streamWriter.writeEndElement();
		streamWriter.writeCharacters(ONETAB);
	}

	private void buildElement(XMLStreamWriter streamWriter, String name, String value, String tab) throws XMLStreamException {

		streamWriter.writeCharacters(tab);
		streamWriter.writeStartElement(name);
		streamWriter.writeCharacters(value);
		streamWriter.writeEndElement();
	}
}