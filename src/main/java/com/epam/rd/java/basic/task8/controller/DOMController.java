package com.epam.rd.java.basic.task8.controller;


import com.epam.rd.java.basic.task8.step1.ChargerStation;
import com.epam.rd.java.basic.task8.step1.ChargerStations;
import com.epam.rd.java.basic.task8.step1.VisualParameters;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.imageio.IIOException;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.*;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Comparator;

/**
 * Controller for DOM parser.
 */
public class DOMController {

	private String xmlFileName;

	public DOMController(String xmlFileName) {
		this.xmlFileName = xmlFileName;
	}

	public ChargerStations parseXML() throws IOException, SAXException, ParserConfigurationException {
				ChargerStations cs = new ChargerStations();
				DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
				//factory.setAttribute();

				DocumentBuilder builder = factory.newDocumentBuilder();
				Document document = builder.parse(this.xmlFileName);
				NodeList nl = document.getElementsByTagName("chargingStation");
				for (int i=0; i < nl.getLength(); i++){
					Node n = nl.item(i);
			if(n.hasChildNodes()){
				cs.getList().add(parseNode(n.getChildNodes()));
			}
		}
		return cs;
	}

	private ChargerStation parseNode(NodeList childNode){
		return new ChargerStation(
				childNode.item(1).getTextContent(),
				childNode.item(3).getTextContent(),
				childNode.item(5).getTextContent(),
				getvP(childNode.item(7).getChildNodes())
		);

	}

	private VisualParameters getvP(NodeList childNode) {
		return new VisualParameters(
				childNode.item(1).getTextContent(),
				childNode.item(3).getTextContent()
		);
	}

	public void Sorting (ChargerStations chargerStations){
		chargerStations.getList().sort(Comparator.comparing(ChargerStation::getName, Comparator.reverseOrder()));
	}

	public void createXML(ChargerStations chargerStations, String outputXmlFile) throws ParserConfigurationException, TransformerException, IOException {
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		//factory.setFeature("http://apache.org/xml/features/", true);
		factory.setFeature("http://apache.org/xml/features/disallow-doctype-decl", true);
		factory.setNamespaceAware(true);
		DocumentBuilder builder = factory.newDocumentBuilder();
		Document document = builder.newDocument();
		Element root = document.createElement("chargingStations");
		root.setAttribute("xmlns", "http://www.nd.ua");
		root.setAttribute("xmlns:xsi", "http://www.w3.org/2001/XMLSchema-instance");
		root.setAttribute("xsi:schemaLocation", "http://www.nd.ua input.xsd ");
		Element element;
		for (ChargerStation cs: chargerStations.getList()) {
			Element chargingStation = document.createElement("chargingStation");
			createElement(document,chargingStation, "name", cs.getName());
			createElement(document,chargingStation, "type", cs.getType().getName());
			createElement(document,chargingStation, "brand", cs.getBrand());
			createVP(document,chargingStation,cs);
			root.appendChild(chargingStation);
			System.out.println(cs);
		}
		document.appendChild(root);
		Transformer transformer = TransformerFactory.newInstance().newTransformer();
		transformer.setOutputProperty(OutputKeys.INDENT, "yes");

		DOMSource source = new DOMSource(document);
		StreamResult result = new StreamResult(new FileWriter(outputXmlFile));
		transformer.transform(source, result);

	}

	private void createElement (Document document,Element parent, String tag, String value){
		Element element = document.createElement(tag);
		element.setTextContent(value);
		parent.appendChild(element);
	}

	private void createVP (Document document,Element parent, ChargerStation cs){
		Element element = document.createElement("visualParameters");
		createElement(document,element, "topColour", cs.getvP().getTopColour());
		createElement(document,element, "bottomColour", cs.getvP().getBottomColour());
		parent.appendChild(element);
	}
}
