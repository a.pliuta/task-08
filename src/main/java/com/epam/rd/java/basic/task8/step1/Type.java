package com.epam.rd.java.basic.task8.step1;

public enum Type {TYPE1("Type1"),TYPE2("Type2"),CHADEMO("Chademo");
    private String name;

    Type(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

}
