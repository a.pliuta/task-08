package com.epam.rd.java.basic.task8.controller;

import org.xml.sax.SAXException;
import org.xml.sax.SAXNotRecognizedException;
import org.xml.sax.SAXNotSupportedException;

import javax.xml.XMLConstants;
import javax.xml.transform.Source;
import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import javax.xml.validation.Validator;
import java.io.File;
import java.io.IOException;

public class ValidatorXML {
    public static void validation(String XMLfile, String XSDfile) throws SAXException {
        StreamSource XMLStream = new StreamSource(XMLfile);
        StreamSource XSDStream = new StreamSource(XSDfile);

        SchemaFactory factory = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
        factory.setProperty(XMLConstants.ACCESS_EXTERNAL_DTD, "");
        factory.setProperty(XMLConstants.ACCESS_EXTERNAL_SCHEMA, "");
        try {
            Schema schema = factory.newSchema(XSDStream);
            Validator validator = schema.newValidator();
            validator.validate(XMLStream);
            System.out.println(XMLfile + " validating is ended correctly");
        } catch (SAXException | IOException e) {
            throw new SAXException(XMLStream.getSystemId() + " validating is not ended");
        }
    }

}
