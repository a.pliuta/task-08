package com.epam.rd.java.basic.task8.step1;

public class VisualParameters {
    private String topColour;
    private String bottomColour;

    public VisualParameters() {
    }

    public VisualParameters(String topColour, String bottomColour) {
        this.topColour = topColour;
        this.bottomColour = bottomColour;
    }

    public String getTopColour() {
        return topColour;
    }

    public void setTopColour(String topColour) {
        this.topColour = topColour;
    }

    public String getBottomColour() {
        return bottomColour;
    }

    public void setBottomColour(String bottomColour) {
        this.bottomColour = bottomColour;
    }

    @Override
    public String toString() {
        return "VisualParameters{" +
                "topColour='" + topColour + '\'' +
                ", bottomColour='" + bottomColour + '\'' +
                '}';
    }
}
