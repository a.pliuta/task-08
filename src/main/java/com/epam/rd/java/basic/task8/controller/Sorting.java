package com.epam.rd.java.basic.task8.controller;

import com.epam.rd.java.basic.task8.step1.ChargerStation;
import com.epam.rd.java.basic.task8.step1.ChargerStations;

import java.util.Comparator;

public class Sorting {
    public void Sorting2 (ChargerStations chargerStations){
        chargerStations.getList().sort(Comparator.comparing((ChargerStation::getName)));
    }
}
