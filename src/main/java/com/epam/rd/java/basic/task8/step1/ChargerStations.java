package com.epam.rd.java.basic.task8.step1;

import java.util.ArrayList;
import java.util.List;

public class ChargerStations {
    List<ChargerStation> list = new ArrayList<>();

    public List<ChargerStation> getList() {
        return list;
    }

    public void setList(List<ChargerStation> list) {
        this.list = list;
    }

    @Override
    public String toString() {
        return "ChargerStations{" +
                "list=" + list +
                '}';
    }
}
